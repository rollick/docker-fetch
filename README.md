# fetch

Simple image to fetch a tar.gz, uncompress and run an executable from the file.

# env variables

$FETCH_URL is the url to fetch (required)
$TAR_ARGS are arguments to pass to the tar command (optional)
