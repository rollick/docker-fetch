#!/bin/sh

curl -sSL $FETCH_URL > /fetch.tar.gz
cd /app
tar zxf /fetch.tar.gz $TAR_ARGS

exec "$@"
