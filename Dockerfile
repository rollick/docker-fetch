FROM alpine
RUN apk --update --no-cache add curl ca-certificates tar xz busybox-extras
WORKDIR /app
COPY fetch.sh /fetch.sh
RUN chmod +x /fetch.sh
ENTRYPOINT ["/fetch.sh"]
CMD ["ls", "/app"]
